module.exports = {
  apps: [
    {
      name: 'dataloader',
      script: './dist/main.js',
      instances: 'max',
      max_memory_restart: '512M',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
};
