import { Injectable } from '@nestjs/common';
import { UserInputError } from 'apollo-server-express';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCategoryInput } from './dto/create-category.input';
import { UpdateCategoryInput } from './dto/update-category.input';
import { Category } from './entity/category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private readonly repository: Repository<Category>,
  ) {}

  async create(createCategoryInput: CreateCategoryInput) {
    const category = this.repository.create(createCategoryInput);
    await category.save();
    return category;
  }

  async findAll() {
    return await this.repository.find();
  }

  async findOne(id: number) {
    const category = await this.repository.findOne(id);
    if (!category) throw new UserInputError('Category not found');
    return category;
  }

  async update(id: number, updateCategoryInput: UpdateCategoryInput) {
    const oldCategory = await this.findOne(id);
    const updateCategory = {
      ...oldCategory,
      ...updateCategoryInput,
    };
    await this.repository.update(id, updateCategory);
    return updateCategory;
  }

  remove(id: number) {
    return `This action removes a #${id} category`;
  }

  async findByIds(ids: number[]) {
    console.log(ids);
    return await this.repository.findByIds(ids);
  }
}
