export const databaseConfig: IDatabaseConfig = {
  type: process.env.DB_TYPE || 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: parseInt(process.env.DB_PORT) || 5432,
  user: process.env.DB_USER || 'postgres',
  password: process.env.DB_PASSWORD || 'secret',
  name: process.env.DB_NAME || 'dataloader',
};

interface IDatabaseConfig {
  user: string;
  password: string;
  port: number;
  name: string;
  host: string;
  type: any;
  cache?: boolean;
}
