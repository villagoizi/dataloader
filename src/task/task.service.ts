import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserInputError } from 'apollo-server-express';
import { Repository } from 'typeorm';
import { CreateTaskInput } from './dto/create-task.input';
import { UpdateTaskInput } from './dto/update-task.input';
import { Task } from './entity/task.entity';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task) private readonly repository: Repository<Task>,
  ) {}

  async create(createTaskInput: CreateTaskInput) {
    const task = this.repository.create(createTaskInput);
    await task.save();
    return task;
  }

  async findAll() {
    return await this.repository.find();
  }

  async findOne(id: number) {
    const task = await this.repository.findOne(id);
    if (!task) throw new UserInputError('Task not found');
    return task;
  }

  update(id: number, updateTaskInput: UpdateTaskInput) {
    return `This action updates a #${id} task`;
  }

  async remove(id: number) {
    const task = await this.findOne(id);
    await task.remove();
    return task;
  }
}
