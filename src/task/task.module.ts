import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskResolver } from './task.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task } from './entity/task.entity';
import { CategoryLoader } from 'src/dataloaders/category.dataloader';
import { CategoryModule } from 'src/category/category.module';

@Module({
  imports: [TypeOrmModule.forFeature([Task]), CategoryModule],
  providers: [TaskResolver, TaskService, CategoryLoader],
})
export class TaskModule {}
