import { Injectable, Scope } from '@nestjs/common';
import * as DataLoader from 'dataloader';
import { CategoryService } from 'src/category/category.service';

@Injectable({ scope: Scope.DEFAULT })
export class CategoryLoader {
  constructor(private categoryService: CategoryService) {}

  public readonly batchCategories = new DataLoader(
    async (categoriesIds: number[]) => {
      const categories = await this.categoryService.findByIds(categoriesIds);
      const categoryMap = new Map(categories.map((c) => [c.id, c]));
      return categoriesIds.map((categoryId) => categoryMap.get(categoryId));
    },
  );
}
